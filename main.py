import pygame
import time
from Robot import Robot
import math

pygame.init()
screen = pygame.display.set_mode((1000, 500))
done = False

t = 0
G = 9.81
dt = 0.01

vy = 0
sy0 = 0
sy = 0


def grad_to_rad(grad):
    return grad / 360 * math.pi * 2


robot1 = Robot(10, grad_to_rad(30))

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    while sy >= 0:
        vx = robot1.calc_vx(G, t)
        vy = robot1.calc_vy(G, t)
        sy = robot1.calc_sy(G, t)
        if sy < 0:
            break
        print("Speed x: ", vx, " Speed y: ", vy, " Coordinate: ", sy)

        screen.fill((0, 0, 0))

        pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(5 + vx * 10, 500 - sy * 10, 10, 10))
        pygame.display.flip()
        t += dt
        time.sleep(dt)

        #blabla
