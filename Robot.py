from math import sin, cos


class Robot:
    def __init__(self, v0, angle):  # Angle in digrees
        self.v0 = v0
        self.angle = angle

    def calc_vx(self, G, t):
        return self.v0 * cos(self.angle) + G * t

    def calc_vy(self, G, t):
        return self.v0 * sin(self.angle) + G * t

    def calc_sy(self, G, t):
        return self.v0 * t - (G * t ** 2) / 2
